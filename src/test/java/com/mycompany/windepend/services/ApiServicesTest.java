///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.mycompany.windepend.services;
//
//import com.mycompany.windepend.models.Coordinate;
//import com.mycompany.windepend.models.Location;
//import com.mycompany.windepend.models.MeteoData;
//import java.io.IOException;
//import java.util.List;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//
///**
// *
// * @author Peti
// */
//public class ApiServicesTest {
//    
//    public ApiServicesTest() {
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//    
//    @Before
//    public void setUp() {
//    }
//    
//    @After
//    public void tearDown() {
//    }
//
//    /**
//     * Test of getMeteoDataFromWeatherBit method, of class ApiServices.
//     */
//    @Test
//    public void testGetMeteoDataFromWeatherBit() throws IOException {
//        Location loc = new Location();
//        Coordinate c = new Coordinate();
//        c.setLat(47.706787);
//        c.setLon(18.693920);
//        loc.setCoordinate(c);
//        ApiServices instance = new ApiServices();
//        
//        MeteoData result = instance.getMeteoDataFromWeatherBit(loc);
//        System.out.println(result.getPressure());
//        System.out.println(result.getSunrise());
//        System.out.println(result.getWind_dir());
//        assertEquals(result.getPressure(), 0, 0);
//    }
//
//    /**
//     * Test of getOpenWeatherCurrentData method, of class ApiServices.
//     */
//    @Test
//    public void testGetOpenWeatherCurrentData() {
//        System.out.println("getOpenWeatherCurrentData");
//        Location location = null;
//        ApiServices instance = new ApiServices();
//        MeteoData expResult = null;
//        MeteoData result = instance.getOpenWeatherCurrentData(location);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of trial method, of class ApiServices.
//     */
//    @Test
//    public void testTrial() throws Exception {
//        System.out.println("trial");
//        Location loc = new Location();
//        Coordinate c = new Coordinate();
//        c.setLat(47.55581);
//        c.setLon(19.00279);
//        loc.setCoordinate(c);
//        ApiServices instance = new ApiServices();
//        
//        List<MeteoData> result = instance.trial(loc);
//        System.out.println(result.get(0).getHumidity());
//        System.out.println(result.get(0).getLast_observation());
//        assertEquals(result.get(0).getHumidity(), 73);
//        assertEquals(result.get(1).getWind_spd(), 1.61, 0);
//        // TODO review the generated test code and remove the default call to fail.
//
//    }
//    
//}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaces;

import com.mycompany.windepend.models.Location;
import com.mycompany.windepend.models.MeteoData;
import java.util.List;

/**
 *
 * @author Your Name <csana at progmatic>
 */
public interface FlightLogic {

    public MeteoData avgMeteoData(List<MeteoData> inputData_measured);

    public boolean evaluateWindSpeed(MeteoData data);

    public boolean evaluateWindDirection(MeteoData data, Location location);

    public boolean evaluatePrecipitation(MeteoData data);

    public boolean evaluateTemperature(MeteoData data);

    public boolean evaluateVisibility(MeteoData data);

    public boolean evaluateForecast(MeteoData data);

    public void possibleCurrentLocations(); //apiservice autowire ??
}

package com.mycompany.windepend.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.windepend.models.Location;
import com.mycompany.windepend.models.MeteoData;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TimeZone;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Peti
 */
@Service
public class ApiServices {

    String openWeatherApiKey = "706e9df84fea533e42eb25f2e5701404";

    public MeteoData getMeteoDataFromWeatherBit(Location location) throws IOException {

        double lon = location.getCoordinate().getX();

        double lat = location.getCoordinate().getY();

        RestTemplate rt = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        ResponseEntity<String> response = rt.getForEntity(
                "https://api.weatherbit.io/v2.0/current?&lat=" + lat + "&lon=" + lon
                + "&key=2b6aaac2226340e7a4a5e5e969be7da7",
                String.class
        );

        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(response.getBody());
        JsonNode list = root.path("data");

        MeteoData meteoData = new MeteoData();
        for (JsonNode jsonNode : list) {

            meteoData.setLocation(location);
            meteoData.setSunrise((LocalTime.parse(jsonNode.path("sunrise").asText())));
            meteoData.setSunset((LocalTime.parse(jsonNode.path("sunset").asText())));
            meteoData.setLast_observation((LocalDateTime.ofInstant(Instant.ofEpochSecond(
                    jsonNode.path("last_observation").asLong()), TimeZone.getDefault().toZoneId())));
            meteoData.setPressure(jsonNode.path("pres").asDouble());
            meteoData.setSlp(jsonNode.path("slp").asDouble());
            meteoData.setWind_spd(jsonNode.path("wind_spd").asDouble());
            meteoData.setWind_dir(jsonNode.path("wind_dir").asInt());
            meteoData.setWind_gust(jsonNode.path("wind_gust").asDouble());
            meteoData.setWind_cdir(jsonNode.path("wind_cdir").asText());
            meteoData.setWind_cdir_full(jsonNode.path("wind_cdir_full").asText());
            meteoData.setTemp(jsonNode.path("temp").asDouble());
            meteoData.setHumidity(jsonNode.path("rh").asInt());
            meteoData.setDewpt(jsonNode.path("dewpt").asInt());
            meteoData.setClouds(jsonNode.path("clouds").asInt());
            meteoData.setVisibility(jsonNode.path("vis").asInt() * 1000);
            meteoData.setPrecipitation(jsonNode.path("precip").asDouble());
            meteoData.setSnow(jsonNode.path("snow").asInt());
            meteoData.setUv(jsonNode.path("uv").asInt());
            meteoData.setDhi(jsonNode.path("dhi").asText());
            meteoData.setDni(jsonNode.path("dni").asText());
            meteoData.setGhi(jsonNode.path("ghi").asText());
            meteoData.setSolar_rad(jsonNode.path("solar_rad").asText());
            meteoData.setElev_angle(jsonNode.path("elev_angle").asInt());
            meteoData.setH_angle(jsonNode.path("h_angle").asInt());
        }

        return meteoData;

    }

    public List<MeteoData> getOpenWeatherForecastData(Location loc) throws IOException {

        List<MeteoData> metDataList = new ArrayList<>();

        RestTemplate rt = new RestTemplate();
        double longitude = loc.getCoordinate().getX();
        double latitude = loc.getCoordinate().getY();

        ResponseEntity<String> response = rt.getForEntity("http://api.openweathermap.org/data/2.5/forecast?lat=" + latitude
                + "&lon=" + longitude
                + "&appid=" + openWeatherApiKey + "&units=metric", String.class);

        //JSON Object Mapper  
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(response.getBody());

        JsonNode list = root.path("list");
        for (JsonNode jsonNode : list) {

            MeteoData metdata = new MeteoData();
            Long dt = jsonNode.path("dt").asLong();
            Long sunrise = jsonNode.path("sys").path("sunrise").asLong();
            Long sunset = jsonNode.path("sys").path("sunset").asLong();
            double pressure = jsonNode.path("main").path("pressure").asDouble();
            double wind_spd = jsonNode.path("wind").path("speed").asDouble();
            int wind_dir = jsonNode.path("wind").path("deg").asInt();
            double wind_gust = jsonNode.path("wind").path("gust").asDouble();
            double temp = jsonNode.path("main").path("temp").asDouble();
            int humidity = jsonNode.path("main").path("humidity").asInt();
            int clouds = jsonNode.path("clouds").path("all").asInt();
            double precipitation = jsonNode.path("rain").asDouble();
            int snow = jsonNode.path("snow").asInt();
            String foreCastTime = jsonNode.path("dt_txt").asText();
            DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalTime time = LocalDateTime.parse(foreCastTime, fmt).toLocalTime();

            //Filling Metdata
            LocalDateTime forecastDate = LocalDateTime.ofInstant(Instant.ofEpochSecond(dt), TimeZone.getDefault().toZoneId());
            LocalDateTime sunsetToLDT = LocalDateTime.ofInstant(Instant.ofEpochSecond(sunset), TimeZone.getDefault().toZoneId());
            LocalDateTime sunriseToLDT = LocalDateTime.ofInstant(Instant.ofEpochSecond(sunrise), TimeZone.getDefault().toZoneId());
            metdata.setForcastTime(time);
            metdata.setLast_observation(forecastDate);
            metdata.setPressure(pressure);
            metdata.setWind_spd(wind_spd);
            metdata.setWind_dir(wind_dir);
            metdata.setWind_gust(wind_gust);
            metdata.setTemp(temp);
            metdata.setHumidity(humidity);
            metdata.setClouds(clouds);
            metdata.setPrecipitation(precipitation);
            metdata.setSnow(snow);
            metDataList.add(metdata);
        }
        return metDataList;
    }
}

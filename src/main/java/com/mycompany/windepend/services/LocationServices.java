/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.windepend.services;

import com.mycompany.windepend.DataTransferObjects.LocationDTO;
import com.mycompany.windepend.models.Location;
import com.mycompany.windepend.models.MeteoData;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import java.io.IOException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author én
 */
@Service
public class LocationServices {

    @PersistenceContext
    EntityManager em;

    @Autowired
    ApiServices as;

    @Transactional
    public boolean putNewLocationToDatabase(Location l) {    
   
        em.persist(l);
        return true;
        
    }

    @Transactional
    public void putNewLocationToDatabaseFromXML(Location l) {
        
        em.persist(l);
        
    }
    
    public List<Location> listAllLocations() {
        List<Location> locations = em.createQuery("SELECT l FROM Location l order by isAvailable desc")
                .getResultList();
        return locations;
    }

    public List<MeteoData> locationFinder(Location loc) {
        Location location = em.find(Location.class, loc);
        List<MeteoData> actual = location.getActual();
        return actual;

    }
    
    @Transactional
    public void putAPIdataToLocations() throws IOException {
        List<Location> locations = em.createQuery("SELECT l FROM Location l")
                .getResultList();
        for (Location location : locations) {

            if (location.getActual().size() > 20) {
                location.getActual().remove(0);

            }
            location.getActual().add(as.getMeteoDataFromWeatherBit(location));
            em.persist(location);
        }
    }
    
    
    @Transactional
    public void putForcastAPI1dataToLocations() throws IOException {
        List<Location> locations = em.createQuery("SELECT l FROM Location l")
                .getResultList();
        for (Location location : locations) {

            
            location.getForecast().add(0, as.getOpenWeatherForecastData(location).get(0));
            location.getForecast().add(1, as.getOpenWeatherForecastData(location).get(1));
            em.persist(location);
        }
    }
    

    @Transactional
    public void locationAdder() {

        GeometryFactory gf = new GeometryFactory();

        Location l0 = new Location();

        Coordinate c = new Coordinate(47.55581, 19.00279);
        Point coord = gf.createPoint(c);
        l0.setName("Óbuda");
        l0.setAMSL(440);
        l0.setCoordinate(coord);
        l0.setHeading(45);
        l0.setMax_limit(90);
        l0.setMin_limit(0);

        putNewLocationToDatabase(l0);

        Location l1 = new Location();

        Coordinate c1 = new Coordinate(47.55215, 18.98929);
        Point coord1 = gf.createPoint(c1);
        l1.setName("Újlaki");
        l1.setAMSL(420);
        l1.setCoordinate(coord1);
        l1.setHeading(315);
        l1.setMax_limit(345);
        l1.setMin_limit(285);

        putNewLocationToDatabase(l1);

        Location l2 = new Location();

        Coordinate c2 = new Coordinate(47.68868, 18.70899);
        Point coord2 = gf.createPoint(c2);
        l2.setName("Mókus");
        l2.setAMSL(300);
        l2.setCoordinate(coord2);
        l2.setHeading(170);
        l2.setMax_limit(200);
        l2.setMin_limit(140);

        putNewLocationToDatabase(l2);

        Location l3 = new Location();

        Coordinate c3 = new Coordinate(47.68868, 18.70899);
        Point coord3 = gf.createPoint(c3);
        l3.setName("Pilis Partizán");
        l3.setAMSL(630);
        l3.setCoordinate(coord3);
        l3.setHeading(50);
        l3.setMax_limit(80);
        l3.setMin_limit(20);

        putNewLocationToDatabase(l3);

        Location l4 = new Location();

        Coordinate c4 = new Coordinate(47.68868, 18.70899);
        Point coord4 = gf.createPoint(c4);
        l4.setName("Pilis Nyugat Délnyugat");
        l4.setAMSL(660);
        l4.setCoordinate(coord4);
        l4.setHeading(225);
        l4.setMax_limit(255);
        l4.setMin_limit(190);

        putNewLocationToDatabase(l4);

        Location l5 = new Location();

        Coordinate c5 = new Coordinate(47.68868, 18.70899);
        Point coord5 = gf.createPoint(c5);
        l5.setName("Fedémes");
        l5.setAMSL(250);
        l5.setCoordinate(coord5);
        l5.setHeading(180);
        l5.setMax_limit(0);
        l5.setMin_limit(360);

        putNewLocationToDatabase(l5);

    }

    @Transactional
    public boolean deleteLocation(Long id) {
        Location locationToDelete = em.find(Location.class, id);
        em.remove(locationToDelete);
        return true;
    }

    @Transactional
    public boolean modifyLocation(long id, LocationDTO dto) {

        Location locationToModify = em.find(Location.class, id);

        if (dto.getAMSL() != null) {
            locationToModify.setAMSL(dto.getAMSL());
        }
        if (dto.getHeading() != null) {
            locationToModify.setHeading(dto.getHeading());
        }

        if (dto.getMax_limit() != null) {
            locationToModify.setMax_limit(dto.getMax_limit());

        }
        if (dto.getMin_limit() != null) {
            locationToModify.setMin_limit(dto.getMin_limit());
        }
        if (dto.getName() != null) {
            locationToModify.setName(dto.getName());
        }

        em.persist(locationToModify);
        return true;
    }
    
     @Transactional
    public void setAvailabilityTrue(Location location) {
        location.setIsAvailable(true);
        em.persist(location);
    }
    
    @Transactional
    public void setAvailabilityFalse(Location location) {
        location.setIsAvailable(false);
        em.persist(location);
        
    }
    
     @Transactional
    public void setAvailabilityForcast0True(Location location) {
        location.setIsAvailableForcast0(true);
        em.persist(location);
    }
    
    @Transactional
    public void setAvailabilityForcast0False(Location location) {
        location.setIsAvailableForcast0(false);
        em.persist(location);
        
    }
     @Transactional
    public void setAvailabilityForcast1True(Location location) {
        location.setIsAvailableForcast1(true);
        em.persist(location);
    }
    
    @Transactional
    public void setAvailabilityForcast1False(Location location) {
        location.setIsAvailableForcast1(false);
        em.persist(location);
    }   


    public Location findLocationById(Long id) {
        return em.find(Location.class, id);
    }

}

package com.mycompany.windepend.services;

import com.mycompany.windepend.models.User;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author hlasznyedit
 */
@Service
public class UserService implements UserDetailsService {

    @PersistenceContext
    private EntityManager em;

    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            User user = (User) em.createQuery("SELECT au FROM User au WHERE au.username = :username")
                    .setParameter("username", username)
                    .getSingleResult();
            return user;
        } catch (NoResultException e) {
            throw new UsernameNotFoundException(username);
        }
    }

    public boolean userExists(String username) {
        List<Object> results = em.createNamedQuery("loadUserByUsername")
                .setParameter("username", username)
                .getResultList();
        return !results.isEmpty();
    }

    @Transactional
    public User createUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        em.persist(user);
        return user;
    }
    
    public List<User> getUsers() {
        List<User> users = em.createQuery("SELECT u FROM User u").getResultList();
        return users;
    }

}   //  end of class UserService

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.windepend.services;

import com.mycompany.windepend.models.Feedback;
import com.mycompany.windepend.models.Location;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author lacikiska
 */
@Service
public class FeedbackService {

    @PersistenceContext
    EntityManager em;

    private static final long TIMERANGE = 6;
    private LocalDateTime hoursAgo = LocalDateTime.now().minusHours(TIMERANGE);

    public long getTimeRange() {
        return TIMERANGE;
    }

    public LocalDateTime getHoursAgo() {
        return hoursAgo;
    }

    @Transactional
    public boolean createFeedback(Feedback f) {
        em.persist(f);
        return true;

    }

    @Transactional
    public List<Feedback> getFeedbackForLocation(Long locationId) {
        List<Feedback> fbList = em.createQuery("Select f From Feedback f Where f.timestamp > :sixHoursAgo "
                + "And f.location.locationId= :locId")
                .setParameter("sixHoursAgo", hoursAgo)
                .setParameter("locId", locationId)
                .getResultList();
        return fbList;
    }

    public double distance(double lat1, double lon1, double lat2,
            double lon2, double el1, double el2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }
}

package com.mycompany.windepend.models;

import com.mycompany.interfaces.FlightLogic;
import com.mycompany.windepend.services.LocationServices;
import com.vividsolutions.jts.geom.GeometryFactory;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Component;

/**
 *
 * @author Your Name <csana at progmatic>
 */
@Component
public class MeteoLogic implements FlightLogic {

    private LocationServices ls;
    private GeometryFactory gf;
    private static final Integer MAX_PRECIPITATION = 3; // mm/h
    private static final Integer MIN_VISIBILITY = 1000;       // meter
    private List<Location> possibleLocations = new ArrayList<>();
    private static final Integer MIN_WIND_SPEED = 1;  // m/s
    private static final Integer MAX_WIND_SPEED = 12;   // m/s

    @Autowired
    public MeteoLogic(LocationServices ls) {
        this.ls = ls;
    }

    public List<MeteoData> MeteoDataMiner(Location loc) {
        return ls.locationFinder(loc);
    }

    @Override
    public MeteoData avgMeteoData(List<MeteoData> inputData_measured) {
        int wind_dir;
        int avg_wind_dir = 0;
        for (MeteoData meteoData : inputData_measured) {
            wind_dir = (int) meteoData.getWind_dir();
            avg_wind_dir *= wind_dir;
        }
        int final_avg_wind = avg_wind_dir / inputData_measured.size();
        MeteoData avgMetDat = new MeteoData();
        avgMetDat.setWind_dir(final_avg_wind);
        
        return avgMetDat;
    }

    @Override
    public boolean evaluateWindSpeed(MeteoData data) {
        if (data.getWind_spd() < MAX_WIND_SPEED
                && data.getWind_spd() > MIN_WIND_SPEED) {
            return true;
        }
        return false;
    }

    @Override
    public boolean evaluateWindDirection(MeteoData data, Location location) {
        int currentWindDirection = (int) data.getWind_dir();
        if (location.getMin_limit() > location.getMax_limit()) {
            if ((currentWindDirection > location.getMin_limit() && location.getMax_limit() < 360)
                    || currentWindDirection < location.getMax_limit()) {
                return true;
            }
        }

        if (location.getMin_limit() <= currentWindDirection
                && location.getMax_limit() >= currentWindDirection) {
            return true;
        }
        return false;
    }

    @Override
    public boolean evaluatePrecipitation(MeteoData data
    ) {
        if (data.getPrecipitation() > MAX_PRECIPITATION) {
            return false;
        }
        return true;
    }

    @Override
    public boolean evaluateTemperature(MeteoData data
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean evaluateVisibility(MeteoData data
    ) {
        if (data.getVisibility() > MIN_VISIBILITY) {
            return true;
        }
        return false;
    }

    @Override
    public boolean evaluateForecast(MeteoData data
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void possibleCurrentLocations() {
        List<Location> locations = ls.listAllLocations();
        for (Location location : locations) {
            MeteoData data = location.getActual().get(location.getActual().size() - 1);
//            if (evaluateWindSpeed(data)
//                    && evaluateWindDirection(data, location)
//                    && evaluatePrecipitation(data)
//                    && evaluateVisibility(data)) {
            if (evaluateWindDirection(data, location)
                    &&evaluateWindSpeed(data)
                    && evaluatePrecipitation(data)
                    ) {
                ls.setAvailabilityTrue(location);
            } else {
                ls.setAvailabilityFalse(location);
            }
        }
    }
    
    
    public void possibleForecastLocations() {
        List<Location> locations = ls.listAllLocations();
        for (Location location : locations) {
            
            MeteoData data = location.getForecast().get(0);

            if (evaluateWindDirection(data, location)
                    &&evaluateWindSpeed(data)
                    && evaluatePrecipitation(data)
                    ) {
                ls.setAvailabilityForcast0True(location);
            } else {
                ls.setAvailabilityForcast0False(location);
            }
            MeteoData data1 = location.getForecast().get(1);

            if (evaluateWindDirection(data1, location)
                    &&evaluateWindSpeed(data1)
                    && evaluatePrecipitation(data1)
                    ) {
                ls.setAvailabilityForcast1True(location);
            } else {
                ls.setAvailabilityForcast1False(location);
            }
        }
    }
    
}
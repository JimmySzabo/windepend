package com.mycompany.windepend.models;

import com.vividsolutions.jts.geom.Point;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 *
 * @author merthen
 */
@Entity
public class Location {

    @Id
    @GeneratedValue
    private long locationId;
    @ManyToMany(mappedBy = "favorites")
    private List<User> favoritedBy;

    private String name;

    private Point coordinate;

    private int heading;

    private int max_limit;

    private int min_limit;

    private int AMSL;
    
    @Column(length = 8000)
    private String description;

    private boolean isAvailable;

    private boolean isAvailableForcast0;

    private boolean isAvailableForcast1;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "location")
    private List<MeteoData> actual = new ArrayList<>();
    @OneToMany(cascade = CascadeType.ALL)
    private List<MeteoData> forecast = new ArrayList<>();

    @Transient
    private List<MeteoData> ICON_forcast = new ArrayList<>();
    @Transient
    private List<MeteoData> WRF_forcast = new ArrayList<>();

    @OneToMany(mappedBy = "location")
    private List<Feedback> locationFeedback;

    public List<MeteoData> getForecast() {
        return forecast;
    }

    public void setForecast(List<MeteoData> forecast) {
        this.forecast = forecast;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public long getLocationId() {
        return locationId;
    }

    public void setLocationId(long locationId) {
        this.locationId = locationId;
    }

    public List<User> getFavoritedBy() {
        return favoritedBy;
    }

    public void setFavoritedBy(List<User> favoritedBy) {
        this.favoritedBy = favoritedBy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Point getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Point coordinate) {
        this.coordinate = coordinate;
    }

    public int getHeading() {
        return heading;
    }

    public void setHeading(int heading) {
        this.heading = heading;
    }

    public int getMax_limit() {
        return max_limit;
    }

    public void setMax_limit(int max_limit) {
        this.max_limit = max_limit;
    }

    public int getMin_limit() {
        return min_limit;
    }

    public void setMin_limit(int min_limit) {
        this.min_limit = min_limit;
    }

    public int getAMSL() {
        return AMSL;
    }

    public void setAMSL(int AMSL) {
        this.AMSL = AMSL;
    }

    public boolean isIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public boolean isIsAvailableForcast0() {
        return isAvailableForcast0;
    }

    public void setIsAvailableForcast0(boolean isAvailableForcast0) {
        this.isAvailableForcast0 = isAvailableForcast0;
    }

    public boolean isIsAvailableForcast1() {
        return isAvailableForcast1;
    }

    public void setIsAvailableForcast1(boolean isAvailableForcast1) {
        this.isAvailableForcast1 = isAvailableForcast1;
    }

    public List<MeteoData> getActual() {
        return actual;
    }

    public void setActual(List<MeteoData> actual) {
        this.actual = actual;
    }

    public List<MeteoData> getICON_forcast() {
        return ICON_forcast;
    }

    public void setICON_forcast(List<MeteoData> ICON_forcast) {
        this.ICON_forcast = ICON_forcast;
    }

    public List<MeteoData> getWRF_forcast() {
        return WRF_forcast;
    }

    public void setWRF_forcast(List<MeteoData> WRF_forcast) {
        this.WRF_forcast = WRF_forcast;
    }

    public List<Feedback> getLocationFeedback() {
        return locationFeedback;
    }

    public void setLocationFeedback(List<Feedback> locationFeedback) {
        this.locationFeedback = locationFeedback;
    }

}

package com.mycompany.windepend.models;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author merthen
 */
@Entity
public class UserAuthority implements GrantedAuthority{

    @ManyToMany(mappedBy = "authorities")
    private List<User> users;
    @Id
    private String authority;

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

}   
    
    


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.windepend.models;

import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author lacikiska
 */
@Entity
public class Feedback {

    @Id
    @GeneratedValue
    private Long FeedbackId;
    @ManyToOne
    private User author;
    private String message;
    private LocalDateTime timestamp = LocalDateTime.now();
    @ManyToOne
    private Location location;

    public Feedback() {
    }

    public Feedback(User author, String message, Location location) {
        this.author = author;
        this.message = message;
        this.location = location;
    }

    public Long getFeedbackId() {
        return FeedbackId;
    }

    public void setFeedbackId(Long FeedbackId) {
        this.FeedbackId = FeedbackId;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

}

package com.mycompany.windepend.models;

import java.time.LocalDateTime;
import java.time.LocalTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author merthen
 */
@Entity
public class MeteoData {

    @Id
    @GeneratedValue
    private int id;
    
    private long entryId;
    @ManyToOne
    private Location location;
    private LocalTime sunrise; // Sunrise time (HH:MM).
    private LocalTime sunset;
    private int stationId; //Source station ID.
    private LocalDateTime last_observation; // (YYYY-MM-DD HH:MM). 
    private double pressure;  //Pressure (mb).
    private double slp;  // Sea level pressure (mb).
    private double wind_spd; // Wind speed (Default m/s).
    private double wind_dir; // Wind direction (degrees).
    private double wind_gust;//brief increase in speed of the wind(m/s);
    private String wind_cdir;  // Abbreviated wind direction.
    private String wind_cdir_full; //Verbal wind direction.
    private double temp; //Temperature (default Celcius).
    private int humidity; //Relative humidity (%).
    private int dewpt; // Dew point (default Celcius).
    private int clouds; // Cloud coverage (%).
    private int visibility; // Visibility (default KM).
    private double precipitation; // Liquid equivalent precipitation rate (default mm/hr).
    private int snow; // Snowfall (default mm/hr).
    private int uv; // UVIndex (0-11+)
    private String dhi; // Diffuse horizontal solar irradiance (W/m^2) [Clear Sky]
    private String dni; // Direct normal solar irradiance (W/m^2) [Clear Sky]
    private String ghi; // Global horizontal solar irradiance (W/m^2) [Clear Sky]
    private String solar_rad; // Estimated Solar Radiation (W/m^2).
    private int elev_angle; // Solar elevation angle (degrees).
    private int h_angle; // Solar hour angle (degrees).
    private LocalTime forcastTime;

    public long getEntryId() {
        return entryId;
    }

    public void setEntryId(long entryId) {
        this.entryId = entryId;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public LocalTime getSunrise() {
        return sunrise;
    }

    public void setSunrise(LocalTime sunrise) {
        this.sunrise = sunrise;
    }

    public LocalTime getSunset() {
        return sunset;
    }

    public void setSunset(LocalTime sunset) {
        this.sunset = sunset;
    }

    public int getStationId() {
        return stationId;
    }

    public void setStationId(int stationId) {
        this.stationId = stationId;
    }

    public LocalDateTime getLast_observation() {
        return last_observation;
    }

    public void setLast_observation(LocalDateTime last_observation) {
        this.last_observation = last_observation;
    }

    public double getPressure() {
        return pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public double getSlp() {
        return slp;
    }

    public void setSlp(double slp) {
        this.slp = slp;
    }

    public double getWind_spd() {
        return wind_spd;
    }

    public void setWind_spd(double wind_spd) {
        this.wind_spd = wind_spd;
    }

    public double getWind_dir() {
        return wind_dir;
    }

    public void setWind_dir(double wind_dir) {
        this.wind_dir = wind_dir;
    }

    public double getWind_gust() {
        return wind_gust;
    }

    public void setWind_gust(double wind_gust) {
        this.wind_gust = wind_gust;
    }

    public String getWind_cdir() {
        return wind_cdir;
    }

    public void setWind_cdir(String wind_cdir) {
        this.wind_cdir = wind_cdir;
    }

    public String getWind_cdir_full() {
        return wind_cdir_full;
    }

    public void setWind_cdir_full(String wind_cdir_full) {
        this.wind_cdir_full = wind_cdir_full;
    }

    public double getTemp() {
        return temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public int getDewpt() {
        return dewpt;
    }

    public void setDewpt(int dewpt) {
        this.dewpt = dewpt;
    }

    public int getClouds() {
        return clouds;
    }

    public void setClouds(int clouds) {
        this.clouds = clouds;
    }

    public int getVisibility() {
        return visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public double getPrecipitation() {
        return precipitation;
    }

    public void setPrecipitation(double precipitation) {
        this.precipitation = precipitation;
    }

    public int getSnow() {
        return snow;
    }

    public void setSnow(int snow) {
        this.snow = snow;
    }

    public int getUv() {
        return uv;
    }

    public void setUv(int uv) {
        this.uv = uv;
    }

    public String getDhi() {
        return dhi;
    }

    public void setDhi(String dhi) {
        this.dhi = dhi;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getGhi() {
        return ghi;
    }

    public void setGhi(String ghi) {
        this.ghi = ghi;
    }

    public String getSolar_rad() {
        return solar_rad;
    }

    public void setSolar_rad(String solar_rad) {
        this.solar_rad = solar_rad;
    }

    public int getElev_angle() {
        return elev_angle;
    }

    public void setElev_angle(int elev_angle) {
        this.elev_angle = elev_angle;
    }

    public int getH_angle() {
        return h_angle;
    }

    public void setH_angle(int h_angle) {
        this.h_angle = h_angle;
    }

    public LocalTime getForcastTime() {
        return forcastTime;
    }

    public void setForcastTime(LocalTime forcastTime) {
        this.forcastTime = forcastTime;
    }
    
    

}

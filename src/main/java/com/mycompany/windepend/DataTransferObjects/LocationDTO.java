/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.windepend.DataTransferObjects;

import com.mycompany.windepend.models.MeteoData;
import java.util.ArrayList;
import java.util.List;

import com.mycompany.windepend.models.MeteoData;

/**
 *
 * @author lacikiska
 */
public class LocationDTO {

    private String name;
    private Long locationID;
    private Double lat;
    private Double lon;
    private Integer heading;
    private Integer max_limit;
    private Integer min_limit;
    private Integer AMSL;
    private MeteoData current;   //    private MeteoData averageMeteoData;
    private Boolean isAvailable;
    private boolean isAvailableForcast0;
    private boolean isAvailableForcast1;

    private List<MeteoData> actual = new ArrayList<>();

    private List<MeteoData> forecast = new ArrayList<>();

    public LocationDTO() {
    }

    public LocationDTO(String name, Long locationID, Double lat, Double lon, Integer heading, Integer max_limit, Integer min_limit, Integer AMSL, Boolean isAvailable, MeteoData current) {
        this.name = name;
        this.locationID = locationID;
        this.lat = lat;
        this.lon = lon;
        this.heading = heading;
        this.max_limit = max_limit;
        this.min_limit = min_limit;
        this.AMSL = AMSL;
        this.isAvailable = isAvailable;
        this.current = current;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getLocationID() {
        return locationID;
    }

    public void setLocationID(Long locationID) {
        this.locationID = locationID;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Integer getHeading() {
        return heading;
    }

    public void setHeading(Integer heading) {
        this.heading = heading;
    }

    public Integer getMax_limit() {
        return max_limit;
    }

    public void setMax_limit(Integer max_limit) {
        this.max_limit = max_limit;
    }

    public Integer getMin_limit() {
        return min_limit;
    }

    public void setMin_limit(Integer min_limit) {
        this.min_limit = min_limit;
    }

    public Integer getAMSL() {
        return AMSL;
    }

    public void setAMSL(Integer AMSL) {
        this.AMSL = AMSL;
    }

    public MeteoData getCurrent() {
        return current;
    }

    public void setCurrent(MeteoData current) {
        this.current = current;
    }

    public Boolean getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(Boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public boolean isIsAvailableForcast0() {
        return isAvailableForcast0;
    }

    public void setIsAvailableForcast0(boolean isAvailableForcast0) {
        this.isAvailableForcast0 = isAvailableForcast0;
    }

    public boolean isIsAvailableForcast1() {
        return isAvailableForcast1;
    }

    public void setIsAvailableForcast1(boolean isAvailableForcast1) {
        this.isAvailableForcast1 = isAvailableForcast1;
    }

    public List<MeteoData> getActual() {
        return actual;
    }

    public void setActual(List<MeteoData> actual) {
        this.actual = actual;
    }

    public List<MeteoData> getForecast() {
        return forecast;
    }

    public void setForecast(List<MeteoData> forecast) {
        this.forecast = forecast;
    }

    @Override
    public String toString() {
        return "LocationDTO{" + "name=" + name + ", locationID=" + locationID + ", lat=" + lat + ", lon=" + lon + ", heading=" + heading + ", max_limit=" + max_limit + ", min_limit=" + min_limit + ", AMSL=" + AMSL + ", current=" + current + ", isAvailable=" + isAvailable + ", isAvailableForcast0=" + isAvailableForcast0 + ", isAvailableForcast1=" + isAvailableForcast1 + ", actual=" + actual + ", forecast=" + forecast + '}';
    }

}

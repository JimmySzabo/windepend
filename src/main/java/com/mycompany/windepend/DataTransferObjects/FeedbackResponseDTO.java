/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.windepend.DataTransferObjects;

import java.time.LocalDateTime;

/**
 *
 * @author lacikiska
 */
public class FeedbackResponseDTO {

    private String authorName;
    private String message;
    private LocalDateTime timestamp;
//    private LocationDTO location;

    public FeedbackResponseDTO() {
    }

    public FeedbackResponseDTO(String authorName, String message, LocalDateTime timestamp) {
        this.authorName = authorName;
        this.message = message;
        this.timestamp = timestamp;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

//    public LocationDTO getLocation() {
//        return location;
//    }
//
//    public void setLocation(LocationDTO location) {
//        this.location = location;
//    }
}

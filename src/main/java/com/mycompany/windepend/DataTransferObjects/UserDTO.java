package com.mycompany.windepend.DataTransferObjects;

import com.mycompany.windepend.models.UserAuthority;
import java.util.List;

/**
 *
 * @author merthen
 */
public class UserDTO {

    private long id;
    private String name;
    private String username;
    private String email;
    private List<String> authorities;

    public UserDTO(long id, String name, String username, String email, List<String> authorities) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
        this.authorities = authorities;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<String> authorities) {
        this.authorities = authorities;
    }

}

package com.mycompany.windepend.exceptions;

/**
 *
 * @author hlasznyedit
 */
public class InvalidUserException extends RuntimeException {

    public InvalidUserException() {
    }

    public InvalidUserException(String msg) {
        super(msg);
    }
    
}   //  end of class InvalidUserException

package com.mycompany.windepend.scheduler;

import com.mycompany.windepend.models.MeteoLogic;
import com.mycompany.windepend.services.LocationServices;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class Scheduler {


    private LocationServices ls;
    private MeteoLogic ml;

    public Scheduler() {
    }

    @Autowired

    public Scheduler(LocationServices ls, MeteoLogic ml) {
        this.ls = ls;
        this.ml = ml;
    }


    @Scheduled(fixedRate = 300000)// 5 min
    @Transactional
    public void activeLocations() throws IOException {

        //ls.putAPIdataToLocations();
        //ml.possibleCurrentLocations();


    }
    
    @Scheduled(fixedRate = 60000000)// 1000 min
    @Transactional
    public void forcast() throws IOException {
        ls.putForcastAPI1dataToLocations();
        ml.possibleForecastLocations();
        
        


    }
}

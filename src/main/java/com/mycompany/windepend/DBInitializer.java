/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.windepend;

import com.mycompany.windepend.models.Location;
import com.mycompany.windepend.models.User;
import com.mycompany.windepend.models.UserAuthority;
import com.mycompany.windepend.services.LocationServices;
import com.mycompany.windepend.services.UserService;
import com.mycompany.windepend.startpoints.Startpoint;
import com.mycompany.windepend.startpoints.XMLReaderDOM;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Jimmy <jóvan>
 */
@Component
public class DBInitializer {

    @PersistenceContext
    EntityManager em;

    @Autowired
    UserService userService;
    
    
    @Autowired
    LocationServices ls;

    @Transactional
    public void init() {
        createRoles();
        createUsers();
        creatLocation();
        em.flush();

    }

    private void createRoles() {
        //List<UserAuthority> auths = em.createQuery("select a from user_authority as a").getResultList();
        List auths = em.createQuery("SELECT ua FROM UserAuthority as ua").getResultList();
        if (auths == null || auths.isEmpty()) {
            UserAuthority user = new UserAuthority();
            user.setAuthority("USER");
            em.persist(user);

            UserAuthority admin = new UserAuthority();
            admin.setAuthority("ADMIN");
            em.persist(admin);
        }
    }

    private void creatLocation() {
        List<Location> location= em.createQuery("select u from Location as u").getResultList();
        if (location == null || location.isEmpty()) {
            XMLReaderDOM xml = new XMLReaderDOM();
            List<Startpoint> startpointList = xml.getStartpointList("xml/starthelyek.kml");
                        
            for (Startpoint sp : startpointList) {
                Location loc = new Location() ;
                loc.setName(sp.getName());
                loc.setCoordinate(sp.getPoint());
                loc.setDescription(sp.getDescription());
                   
                ls.putNewLocationToDatabaseFromXML(loc);
                    
            }
            //ls.locationAdder();
        }
    }

    private void createUsers() {
        List<User> users = em.createQuery("select u from User as u").getResultList();

        if (users == null || users.isEmpty()) {
            User u = new User();
            u.setUsername("admin");
            u.setPassword("admin");
            UserAuthority ua = new UserAuthority();
            ua.setAuthority("ADMIN");
            UserAuthority ua2 = new UserAuthority();
            ua2.setAuthority("USER");
            u.setAuthorities(new ArrayList<>());
            u.getAuthorities().add(ua);
            u.getAuthorities().add(ua2);
            userService.createUser(u);

        }

    }

    @EventListener(ContextRefreshedEvent.class)
    public void onAppStartup(ContextRefreshedEvent ev) throws ServletException {
        DBInitializer me = ev.getApplicationContext().getBean(DBInitializer.class);
        me.init();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.windepend.controllers;

import com.mycompany.windepend.DataTransferObjects.LocationDTO;
import com.mycompany.windepend.models.Location;
import com.mycompany.windepend.models.MeteoData;
import java.util.ArrayList;
import java.util.List;
import com.mycompany.windepend.services.LocationServices;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import java.util.LinkedList;
import java.util.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lacikiska
 */
@RestController
public class LocationRestController {

    LocationServices ls;

    @Autowired
    public LocationRestController(LocationServices ls) {
        this.ls = ls;
    }

    @RequestMapping(path = "/setup", method = RequestMethod.GET)
    public boolean fillLocations() {
        ls.locationAdder();
        return true;
    }

    @RequestMapping(path = "/Location", method = RequestMethod.GET)
    public List<LocationDTO> showAllLocations() {

        List<LocationDTO> DTOList = new ArrayList<>();
        List<Location> locationList = ls.listAllLocations();

        for (Location location : locationList) {

            LocationDTO newDTO = new LocationDTO();
            newDTO.setName(location.getName());
            newDTO.setLocationID(location.getLocationId());
            newDTO.setHeading(location.getHeading());
            newDTO.setLat(location.getCoordinate().getCoordinate().x);
            newDTO.setLon(location.getCoordinate().getCoordinate().y);
            newDTO.setAMSL(location.getAMSL());
            newDTO.setMax_limit(location.getMax_limit());
            newDTO.setMin_limit(location.getMin_limit());
            int lastIndexMeteoData = (location.getActual().size() - 1);
            newDTO.setCurrent(location.getActual().get(lastIndexMeteoData));
            newDTO.setIsAvailable(location.isIsAvailable());
            newDTO.setIsAvailableForcast1(location.isIsAvailableForcast1());
            newDTO.setIsAvailableForcast0(location.isIsAvailableForcast0());
            //newDTO.setActual(location.getActual());
            newDTO.getCurrent().setLocation(null);
            newDTO.setForecast(location.getForecast());
            for (MeteoData meteoData : newDTO.getForecast()) {
                meteoData.setLocation(null);
            }
            DTOList.add(newDTO);
        }

        return DTOList;
    }

    @RequestMapping(path = "/Location", method = RequestMethod.POST)
    public boolean setLocation(@RequestBody LocationDTO location) {

        Location l = new Location();
        Coordinate c = new Coordinate(location.getLat(), location.getLon());
        GeometryFactory gf = new GeometryFactory();
        Point coord = gf.createPoint(c);
        l.setName(location.getName());
        l.setAMSL(location.getAMSL());
        l.setCoordinate(coord);
        l.setHeading(location.getHeading());
        l.setMax_limit(location.getMax_limit());
        l.setMin_limit(location.getMin_limit());

        return ls.putNewLocationToDatabase(l);
    }

    @RequestMapping(path = "/Location/{id}", method = RequestMethod.DELETE)
    public boolean deleteLocation(@PathVariable(name = "id") Long id) {

        return ls.deleteLocation(id);
    }

    @RequestMapping(path = "/Location/{id}", method = RequestMethod.PUT) //UNSURE
    public boolean updateLocation(@PathVariable(name = "id") Long id, @RequestBody LocationDTO location) {

        ls.modifyLocation(id, location);

        return true;
    }
    
}
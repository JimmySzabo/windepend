/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.windepend.controllers;

import com.mycompany.windepend.DataTransferObjects.FeedbackRequestDTO;
import com.mycompany.windepend.DataTransferObjects.FeedbackResponseDTO;
import com.mycompany.windepend.models.Feedback;
import com.mycompany.windepend.models.Location;
import com.mycompany.windepend.models.User;
import com.mycompany.windepend.services.FeedbackService;
import com.mycompany.windepend.services.LocationServices;
import com.mycompany.windepend.services.UserService;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lacikiska
 */
@RestController
public class LocationFeedbackRestController {

    FeedbackService fs;
    UserService us;
    LocationServices ls;
    private static final int DISTANCE = 1000; //authorised feedback distance from location(m)

    @Autowired
    public LocationFeedbackRestController(FeedbackService fs, UserService us, LocationServices ls) {
        this.fs = fs;
        this.us = us;
        this.ls = ls;
    }

    @RequestMapping(path = "/feedback/{location}", method = RequestMethod.GET)
    public List<FeedbackResponseDTO> listLocationFeedback(@PathVariable("location") Long locationId) {
        List<Feedback> locationFeedback = fs.getFeedbackForLocation(locationId);
        List<FeedbackResponseDTO> response = new ArrayList<>();

        for (Feedback feedback : locationFeedback) {

            FeedbackResponseDTO dto = new FeedbackResponseDTO(feedback.getAuthor().getUsername(),
                    feedback.getMessage(),
                    feedback.getTimestamp());

            response.add(dto);

        }

        return response;
    }

    @RequestMapping(path = "/feedback", method = RequestMethod.POST)
    public boolean addFeedback(@RequestBody FeedbackRequestDTO dto, BindingResult bindingResult) throws NotAllowedToGiveFeedback {

        //user
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User myuser = (User) us.loadUserByUsername(authentication.getName());
        //creating feedback
        String message = dto.getMessage();
        Location l = ls.findLocationById(dto.getLocationId());
        Feedback feedback = new Feedback(myuser, message, l);
        //user location
        GeometryFactory gf = new GeometryFactory();
        Coordinate coord = new Coordinate(dto.getLat(), dto.getLon());
        Point userLocation = gf.createPoint(coord);

        if (fs.distance(userLocation.getX(),
                userLocation.getY(),
                l.getCoordinate().getX(),
                l.getCoordinate().getY(), 0, 0) > DISTANCE) {
            throw new NotAllowedToGiveFeedback("You are not close enought to to location, you wish to evaluate.");
        }
        return fs.createFeedback(feedback);
    }
}

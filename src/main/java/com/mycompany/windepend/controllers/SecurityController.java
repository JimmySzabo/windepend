package com.mycompany.windepend.controllers;

import com.mycompany.windepend.DataTransferObjects.UserDTO;
import com.mycompany.windepend.models.User;
import com.mycompany.windepend.models.UserAuthority;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author merthen
 */
@RestController
public class SecurityController {

    @GetMapping(path = "/csrftoken")
    public String getCsrfToken(HttpServletRequest request) {
        HttpSessionCsrfTokenRepository repo = new HttpSessionCsrfTokenRepository();
        CsrfToken csrf = repo.loadToken(request);
        return csrf.getToken();
    }

    @GetMapping(path = "/userdata")
    public UserDTO getAuthenticatedUserData() {
        User temp =(User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<UserAuthority> authorities = temp.getAuthorities();
        List<String> authoritiesList = new ArrayList<>();
        for (UserAuthority authority : authorities) {
            authoritiesList.add(authority.getAuthority());
        }
        String email = temp.getEmail();
        String name = temp.getName();
        long userId = temp.getUserId();
        String username = temp.getUsername();
        UserDTO ret = new UserDTO(userId, name, username, email, authoritiesList);
        return ret;
    }
}

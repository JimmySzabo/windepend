package com.mycompany.windepend.controllers;

import com.mycompany.windepend.services.UserService;
import com.mycompany.windepend.exceptions.InvalidUserException;
import com.mycompany.windepend.models.User;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author hlasznyedit
 */

@RestController
public class RegistrationController {
    
    private UserService us;
    private PasswordEncoder pwe;
    
    @Autowired
    public RegistrationController(UserService us, PasswordEncoder pwe) {
        this.us = us;
        this.pwe = pwe;
    }
    
    @RequestMapping(path="/registration", method = RequestMethod.PUT)
    public Long createUser(@Valid User user, 
                           BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new InvalidUserException();
        }
        
        boolean userExists = us.userExists(user.getUsername());
        if (userExists) {
            throw new InvalidUserException("User already exists");
        }
        
        User u = us.createUser(user);
        
        return u.getUserId() ;
    }
    
}   //  end of class RegistrationController

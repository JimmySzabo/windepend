/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.windepend.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author lacikiska
 */
@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE)
public class NotAllowedToGiveFeedback extends Exception {

    public NotAllowedToGiveFeedback(String message) {
        super(message);
    }
}

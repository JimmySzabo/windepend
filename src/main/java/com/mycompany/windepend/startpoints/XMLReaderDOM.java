package com.mycompany.windepend.startpoints;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author hlasznyedit 
 * source:
 * https://www.journaldev.com/898/read-xml-file-java-dom-parser 
 * xml validator:
 * https://www.xmlvalidation.com/
 *
 * Extracted tag data: name, description, point
 */
public class XMLReaderDOM {
   

    public  List<Startpoint> getStartpointList(String filePath) {

        InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream(filePath);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        List<Startpoint> spList = new ArrayList<>();

        try {
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(resourceAsStream);
            doc.getDocumentElement().normalize();
            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
            NodeList nodeList = doc.getElementsByTagName("Placemark");

            //now XML is loaded as Document in memory, lets convert it to Object List
            
            spList = new ArrayList<Startpoint>();            
            for (int i = 0; i < nodeList.getLength(); i++) {         
                if (nodeList.item(i).getNodeType() == Node.ELEMENT_NODE) {

                    Element element = (Element) nodeList.item(i);
                    if (isTagExists("Point", element)) {
                        spList.add(getStartpoint(element));
                    }
                }
            }        

        } catch (SAXException | ParserConfigurationException | IOException e1) {
            e1.printStackTrace();
        }

        return spList;
    }   //  end of method main()

    private  Startpoint getStartpoint(Element element) {

        //XMLReaderDOM domReader = new XMLReaderDOM();
        Startpoint sp = new Startpoint();

        sp.setName(getTagValue("name", element));
        if (isTagExists("description", element)) {
            sp.setDescription(getTagValue("description", element));
        }

        sp.setPoint(getPoint(element));

        return sp;
    }   // end of method getStartpoints()

    private  boolean isTagExists(String tag, Element element) {
        NodeList list = element.getElementsByTagName(tag);
        if (list.getLength() == 0) {
            return false;
        } else {
            return true;
        }
    }

    private  String getTagValue(String tag, Element element) {
        //System.out.println(element);
        NodeList list = element.getElementsByTagName(tag);
        Node n = list.item(0);
        NodeList nodeList = n.getChildNodes();
        Node node = (Node) nodeList.item(0);
        return node.getNodeValue();
    }

    private  Point getPoint(Node node) {
        GeometryFactory gf = new GeometryFactory();

        Point p = null;
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) node;

            String[] coordinates = getTagValue("coordinates", element).split(",");

            Double x = Double.parseDouble(coordinates[0]);
            Double y = Double.parseDouble(coordinates[1]);
            p = gf.createPoint(new Coordinate(x, y));
        }
        return p;
    }

}   //  end of class XMLReaderDOM 

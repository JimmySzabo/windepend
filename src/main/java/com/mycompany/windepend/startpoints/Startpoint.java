package com.mycompany.windepend.startpoints;

import com.vividsolutions.jts.geom.Point;

public class Startpoint {
	
    private String name = "";
    private String description = "";
    private Point point;
    
	public String getName() {
            return name;
	}
	public String getDescription() {
	    return description;
	}
	public Point getPoint() {
	    return point;
	}
	public void setName(String name) {
	    this.name = name;
	}
	public void setDescription(String description) {
	    this.description = description;
	}
	public void setPoint(Point point) {
	    this.point = point;
	}
    
   	@Override
	public String toString() {
   		return "Starthelyek:: Name=" + this.name + " Description=" + 
   	               this.description + " Point=" + this.point;
   	}

}   //  end of class Startpoint 
